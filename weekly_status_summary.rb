require 'gitlab'
require 'yaml'
require 'uri'
require 'date'



class Client
  attr_accessor :private_api_key

  def initialize(private_api_key = nil)
    self.private_api_key = private_api_key
  end

  def get(args)
    `curl -s --header 'private-token: #{private_api_key}' '#{args}'`
  end

  def put(url, data)
    `curl --request PUT -s --header 'private-token: #{private_api_key}' '#{url}' --data '#{data}'`
  end

  def post(url, data)
    `curl --request POST -s --header 'private-token: #{private_api_key}' '#{url}' --data '#{data}'`
  end

  def shell_single_quote_escape(str)
    str.gsub("'", %q{})
  end

  ## TODO add pagination
  def issues(label, created_after, created_before)
    with_pagination do |page|
       get "https://gitlab.com/api/v4/issues?scope=all&labels=#{label}&created_before=#{h created_before}&created_after=#{h created_after}&per_page=100&page=#{page}"
    end
  end

  def get_project_issues(project_id)
    with_pagination do |page|
      get "https://gitlab.com/api/v4/projects/#{h project_id}/issues?scope=all&order_by=created_at&sort=desc&per_page=100&page=#{page}"
    end
  end

  def with_pagination(&block)
    page = 1
    issues = []
    loop do
      r = yield page
      parsed = JSON.parse(r)
      page += 1
      case parsed
      when Array
        case parsed.size
        when 0
          break
        else
          unless parsed.first && parsed.first['iid']
            raise "problem fetching project issues: #{parsed}"
          end
          issues.push(*parsed)
        end
      else
        raise "problem fetcing project issues: #{parsed}"
      end
    end
    issues
  end



  def titles_to_issue_id_mapping(project_id)
    @title_to_issues ||= {}
    @title_to_issues[project_id] ||= get_project_issues(project_id).inject({}) do |memo, issue|
      memo[issue['title']] = issue['iid']
      memo
    end
  end

  def get_issues_by_project_id_and_title(project_id, title)
    titles_to_issue_id_mapping(project_id).select do |(k, v)|
      k == title
    end
  end

  def get_issue(project_id, issue_id)
      r = get "https://gitlab.com/api/v4/projects/#{h project_id}/issues/#{h issue_id}"
  end

  def create_issue(project_id, title, description = '')
    warn "creating new issue with title #{title}"
    post "https://gitlab.com/api/v4/projects/#{h project_id}/issues", "description=#{h description}&title=#{h title}"
  end

  def update_issue(project_id, issue_id, title, description = '', state_event = nil)
    warn "updating existing issue with title #{title}"
    url = "https://gitlab.com/api/v4/projects/#{h project_id}/issues/#{h issue_id}"
    data = "description=#{h description}&title=#{h title}"
    case state_event
    when 'close', 'reopen'
      data += "&state_event=#{h state_event}"
    end
    put url, data
  end

  def close_issue(project_id, issue_id)
    put "https://gitlab.com/api/v4/projects/#{h project_id}/issues/#{h issue_id}?state_event=close"
  end

  def delete_issue(project_id, issue_id)
    delete "https://gitlab.com/api/v4/projects/#{h project_id}/issues/#{h issue_id}"
  end

  private

  def h(*args)
    URI.encode_www_form_component(*args)
  end
end

class Highlight
  attr_accessor :issue, :text
  def initialize(issue, text)
    self.issue = issue
    self.text = text
  end
end

class SummaryReport
  STATUS_LABELS = ['OpsSection::AsyncUpdate', 'OpsSection::Weekly-Update']

  API_TOKEN = ENV['GITLAB_API_PRIVATE_TOKEN']
  PROJECT_ID = 36457911
  def initialize(day=Date.today)
    @day = day
    @gitlab = Client.new(API_TOKEN)
  end

  def issue_title
    raise 'implement in subclass'
  end

  def window
    (start_day..end_day)
  end

  def start_day
    raise 'implement in subclass'
  end

  def end_day
    raise 'implement in subclass'
  end

  def summary_report_issue
    @summary_report_issue ||= begin
      issues = @gitlab.get_issues_by_project_id_and_title(PROJECT_ID, issue_title)
      json = case issues.size
      when 0
        @gitlab.create_issue(PROJECT_ID, issue_title, description)
      when 1
        @gitlab.get_issue(PROJECT_ID, issues.first.last)
      else
        warn "more than one issue with title #{issue_title.inspect}.  Closing duplicates."
        iss = issues.shift
        issue = @gitlab.get_issue(PROJECT_ID, issues.last)
        issues.each do |i|
          @gitlab.close_issue(PROJECT_ID, i['iid'])
        end
        issue
      end
      JSON.parse(json)
    end
  end

  def summary_report_issue_id
    summary_report_issue['iid']
  end

  def update_summary_report_issue
    state_event = closed? ? 'close' : nil
    @gitlab.update_issue(PROJECT_ID, summary_report_issue_id, issue_title, description, state_event)
  end

  def closed?
    (Date.today - 7) > end_day
  end

  def update
    update_summary_report_issue
  end

  def description
    index = 1
    table_of_updates = status_issues.sort_by{|i|i['created_at']}.inject("|   | Issue  | Labels   | Comment Count  | Created | Updated | \n|---|---|---|---|---|---|\n") do |memo, item|
      memo << "| #{index}. |  [#{item['title']}](#{item['web_url']}) | #{format_labels(item['labels'])} | #{item['user_notes_count']} | #{format_date(item['created_at'])} | #{ format_date(item['updated_at'])} |\n"
      index += 1
      memo
    end
    index = 1
    table_highlights = highlights.sort_by{|i|[i.issue['created_at'], i.issue['iid']]}.inject("|   | Highlight | Source | \n|---|---|---|\n") do |memo, item|
      memo << "| #{index}. | #{item.text} | #{item.issue['web_url']}+ |\n"
      index += 1
      memo
    end

    ["## Status Issues", table_of_updates, "## Highlights", table_highlights].join("\n\n")
  end

  def format_labels(labels)
    labels.map {|l| %Q[~"#{l}"] }.join(', ')
  end

  def format_date(date)
    Time.parse(date).strftime("%Y-%m-%d")
  end

  def highlights
    status_issues.inject([]) do |memo, issue|
      memo |= issue['description'].scan(/(^.*HIGHLIGHT\b.*$)/).map do |item|
        Highlight.new(issue, item.first)
      end
    end
  end



  def status_issues
    @issues ||= STATUS_LABELS.inject([]) do |memo, label|
        memo |= @gitlab.issues(label, window.min, window.max)
      end
  end
end

class WeeklySummaryReport < SummaryReport
  def start_day
    @day - @day.wday
  end

  def end_day
    start_day + 6
  end

  def issue_title
    "W#{start_day.strftime('%-V')} Ops Sub-Dept Status Summary (#{start_day} - #{end_day})"
  end
end

class MonthlySummaryReport < SummaryReport
  def start_day
    Date.new(@day.year, @day.month, 1)
  end

  def end_day
    Date.new(@day.year, @day.month, -1)
  end

  def issue_title
    "M#{start_day.month} Ops Sub-Dept Status Summary (#{start_day.strftime('%b %Y')})"
  end
end


class QuarterlySummaryReport < SummaryReport
  def start_day
    first_day_of_quarter(@day)
  end

  def end_day
    first_day_of_quarter(start_day + 95) - 1
  end

  def issue_title
    "#{quarter_name} Ops Sub-Dept Status Summary (#{start_day} - #{end_day})"
  end

  private
  def first_day_of_quarter(day)
    month = case day.month
    when 1, 11..12
      11
    when 2..4
      2
    when 5..7
      5
    when 8..10
      8
    end
    if day.month < month
      Date.new(day.year - 1, month, 1)
    else
      Date.new(day.year, month, 1)
    end
  end

  def quarter_name
    d = first_day_of_quarter(@day)
    case d.month
    when 2
      "FY#{d.year+1} Q1"
    when 5
      "FY#{d.year+1} Q2"
    when 8
      "FY#{d.year+1} Q3"
    when 11
      "FY#{d.year+1} Q4"
    end
  end
end

(0..9).each do |i|
  report = WeeklySummaryReport.new(Date.today - (i * 7))
  report.update
end
(0..5).each do |i|
  report = MonthlySummaryReport.new(Date.today - (i * 30))
  report.update
end
(0..2).each do |i|
  report = QuarterlySummaryReport.new(Date.today - (i * 95))
  report.update
end
