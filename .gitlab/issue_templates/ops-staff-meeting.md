## Intent
To efficently share announcements, initiatives, and feedback within the Ops Sub-dept management team we are experimenting with an async meeting process driven by this issue.


## Tasks

### Opening Tasks


### Announcement Tasks
- [ ] @sgoldstein - Add announcements relevant this week to [agenda](https://docs.google.com/document/d/16Swf5ZdnuUSvVOqbm9jCdKM8DrNsgAcb8LgG9OMOFho/edit#) 
- [ ] @dcroft - Add announcements relevant this week to [agenda](https://docs.google.com/document/d/16Swf5ZdnuUSvVOqbm9jCdKM8DrNsgAcb8LgG9OMOFho/edit#) 
- [ ] @cheryl.li - Add announcements relevant this week to [agenda](https://docs.google.com/document/d/16Swf5ZdnuUSvVOqbm9jCdKM8DrNsgAcb8LgG9OMOFho/edit#) 

### Multi-Modal Communication Tasks

- [ ] @sgoldstein - Post slack update in #doe-ops
- [ ] @dcroft - Post slack update in relevant channels
- [ ] @cheryl.li - Post slack update in relevant channels

### Closing Tasks
- [ ] - @sgoldstein - Review and respond to async discussion in agenda doc 

/assign @sgoldstein @dcroft @cheryl.li

/due a week from Friday
