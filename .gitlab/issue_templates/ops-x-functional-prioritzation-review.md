
## Intent

Perform a monthly review of milestone planning in Ops as part of the [Cross Functional Prioritization](https://about.gitlab.com/handbook/engineering/cross-functional-prioritization/#stagesection-level-review) Process.  This issue is intended to capture the Section level review of priorities planned for the coming milestone.

## Review Goals

The goals of this monthly review are:

1. Understand at a high level how effort is being allocated across new features, security fixes, availability work, performance improvements, bug fixes, etc.
1. Align on a desired allocation.  Are there areas we are underinvesting or overinvesting?
1. Understand and align on tradeoffs.  e.g. If we increase investment in one area what needs to be traded off?


## Pre work

* Review [Issue Types by Milestone](https://app.periscopedata.com/app/gitlab/1042933/Issue-Types-by-Milestone) Dashboard
* Discuss async via comments on this issue. 

## Review questions

_Copy and paste this template into a comment and add your responses.  Feel free to deviate from the template - it is there to provide a jumping off point for discussion but not required to follow that format_


```

* Is the dashboard accurate? Are the number of issues in the dashboard within 5% of the SSOT (the issues themselves)? If not, what needs to be done to correct?

* Are we meeting our target of <1% issues and MR being undefined (no type label)?

* Are there overdue Security or Infradev issues?  Are these prioritized in the coming milestone?

* Are we prioritizing work on long term maintenance, availability, and/or scaling efforts?

* How is the group addressing product investment themes?

* Are customer requests and high MRARR community contributions being addressed?

* Are we addressing all issues needed for FedRamp certification?

* Are our goals realistic?  Are we taking on too much or too little work in this milestone?

* Is the overall allocation of effort optimal?  Are there areas we should be investing more or less?

* What prioritization tradeoffs could/should we make?  

* What project, product, customer risks are worth highlighting?  Are there ways we could mitigate these in planning?

* Are we investing sufficiently in our team?  Are we putting enough time and effort into supporting our team members and keeping our teams healthy?

```

