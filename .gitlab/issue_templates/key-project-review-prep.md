

# Ops Sub-Dept Key Project Review

This issue serves as the prep work for a [key project review meeting](https://about.gitlab.com/handbook/engineering/development/ops/#key-project-review-meetings).  It pulls together information and artifacts related to the project to make it efficient for reviewers to come up to speed on the current status of the project.  This enables us to avoid summarizing status and plans in the review meeting and use that time to collaborate on making the project successful.  Participants in the key project review meetings are expected to review this issue and linked artifacts prior to the sync discussion.

In order to give meeting participants a reasonable amount of time to review materials this issue should be completed and shared at least 48 hours in advance of the review meeting.

## Project Details

1. Project name: TBA
1. Project start date: TBA
1. Expected project completion date: TBA
1. Link to [Project Plan](https://about.gitlab.com/handbook/engineering/development/ops/#key-project-planning): [TBA](#)
1. Link to recent [Showcase Demos](https://about.gitlab.com/handbook/engineering/development/ops/#ops-engineering-showcase): [TBA](#)
1. Link to relevant [Architecture Design Docs](https://docs.gitlab.com/ee/architecture/): [TBA](#)
1. Link to relevant [Async Status Updates](https://about.gitlab.com/handbook/engineering/development/ops/#async-updates-no-status-in-meetings): [TBA](#)

## Actions

* [ ] Title the issue `Key Project Review - Project Name - Date`
* [ ] Populate all items in Project Details Section
* [ ] Add a link to this issue to the Review Meeting Agenda
