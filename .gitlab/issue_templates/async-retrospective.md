Let's have an open dialogue to discuss <SUBJECT> - what went well, what was tricky, and what could be improved so that we can learn from one another, and iterate on this together. 🤝

<!--
Name the title: Async Retrospective: <SUBJECT>
Create 4 headings as separate comments:
## :thumbsup: What went well?
## :disappointed: What didn't go so well?
## :bulb: How can we improve for next time?
## :star2: What praise do you have for the team? :hearts:
-->
