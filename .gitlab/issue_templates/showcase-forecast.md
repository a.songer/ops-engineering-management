Hi everyone :wave: -

This issue is intended to get a forecast from each EM on what they intend to present in [this month's showcase](https://about.gitlab.com/handbook/engineering/development/ops/#ops-engineering-showcase). As noted previously, items changing in this list is fine but the intention here is to understand what each group is targeting at this point.

* ~"group::pipeline authoring"
* ~"group::pipeline execution"
* ~"group::pipeline security" 
* ~"group::runner" 
* ~"devops::package"
* ~"group::environments" 
* ~"group::observability"
* Ai Initiative(s):

## What do I need to do? :thinking: :question:

<details>
<summary>Details here!</summary>

1. Record ![Screenshot_2022-12-19_at_10.24.01_AM](https://gitlab.com/groups/gitlab-com/-/uploads/25de49296bc170983f2a3830c57726f2/Screenshot_2022-12-19_at_10.24.01_AM.png) and share your video with the group on the Unfiltered Channel. **NOTE:** Please set the appropriate visibility in case it isn't public information yet.![Screenshot_2022-12-19_at_10.26.02_AM](https://gitlab.com/groups/gitlab-com/-/uploads/b397e432a9ffd38a9c896298bc5dad4c/Screenshot_2022-12-19_at_10.26.02_AM.png)
1. Please make sure to add an entry to this epic by [creating an issue](https://gitlab.com/gitlab-org/ci-cd/section-showcases/-/issues/new) using this [template](https://gitlab.com/gitlab-org/ci-cd/section-showcases/-/blob/main/.gitlab/issue_templates/Showcase%20Discussion.md). :thinking: :speech_balloon: :pencil:
1. (Optional) - Depending on the number of questions being asked, an AMA-style sync meeting can be set up for team members to verbalize their questions.

Our [handbook](https://about.gitlab.com/handbook/engineering/development/ops/#ops-engineering-showcase) has also been updated to reflect these updates to the format too. :book:
</details>

As a reminder :writing_hand:, since these videos are intended for other team members to be aware of ongoing development work within `Ops` or team members outside of `Ops` that is interfaced with, we encourage you to show things that are in an unfinished state as showing log outputs, terminal window command prompts, how data is being retrieved and represented or API responses are very insightful! :bulb: Frontend UIs or refactors are also great to see! The goal is cross-education to see all the exciting things happening across `Ops` and GitLab. Thank you for your continued support of this showcase! :bow: Please continue to share this with your teams and encourage them to record/share their videos here! :pencil:


/assignee @carolinesimpson @marknuzzo @cheryl.li @nicolewilliams @sgoldstein @crystalpoole @nmezzopera @shampton @nicholasklick
