## Intent
This issue describes how broader group and team members are [facilitating a team member to take meaningful time away from work](https://about.gitlab.com/handbook/product/product-manager-role/#taking-time-off). The intent is that the team member isn't required to "catchup" before and after taking a well-deserved vacation. 

## :calendar: Time-off schedule - April 2023

🦊 = **working**
🌴 = PTO
🌞 = Family & Friends day

| Mon | Tue | Wed | Thu | Fri |
| ------ | ------ | ------ | ------ | ------ |
| | | |  |  |


## :handshake: Responsibilities
Include creating release post items, weekly triage issues, responding to community or customer requests, preparing a Kickoff video, prioritizing issues, responding to clarification in current milestone work, etc.

| Priority | Theme | Context Link | Primary | Secondary |
| -------- | ----- | ------------ | ------- | --------- |
|  | | | | |


## :muscle: Coverage Tasks
Include specific issue links for tasks to be completed. Solution validation issues to progress, kickoff, PI or GC prep, etc.
- [ ] 

## :book: References
Here are some references for how I and my group generally works:
- [Ops Sub-Dept handbook](https://about.gitlab.com/handbook/engineering/development/ops/) 


## :white_check_mark: Issue Tasks

### :o: Opening Tasks
- [ ] Assign to yourself
- [ ] Title the issue `PTO Coverage for NAME from DATE until DATE`
- [ ] Add an issue comment for your :recycle: Retrospective Thread
- [ ] Assign due date for your last PTO day
- [ ] Add any relevant references including direction pages, group handbook pages, etc
- [ ] Fill in the Responsibilities table with broad based responsibilities
- [ ] Fill in the specific Coverage Tasks with distinct items to complete and assignees
- [ ] Assign to anyone with a specific task or responsibility assigned to them
- [ ] Share this issue in your section, stage and group Slack channels
- [ ] Ensure you've assigned tasks via PTO by Deel including assigning some tasks to a relevant #g_ , #s_ or #product slack channel
- [ ] Review the [guidelines to communicate your time off](https://about.gitlab.com/handbook/paid-time-off/#communicating-your-time-off)
- [ ] Ensure your PTO by Deel auto-responder points team members to this issue
- [ ] Update your [GitLab status](https://docs.gitlab.com/ee/user/profile/#current-status) to include a reference to this issue
- [ ] (optional) Consider creating autoresponders for [Gmail](https://support.google.com/mail/answer/25922), [LinkedIn Recruiter](https://www.linkedin.com/help/linkedin/answer/a550614/set-and-edit-away-message-on-linkedin) and other tools you use on your day to day.
- [ ] (optional) Setup Slack apps for your time off, like [Donut](https://about.gitlab.com/company/culture/all-remote/tips/#coffee-chats), [Geekbot](https://help.geekbot.com/en/articles/4311732-how-can-i-schedule-an-out-of-office-period), etc. 

### :x: Closing Tasks
- [ ] Review the [Returning from Time Off ](https://about.gitlab.com/handbook/product/product-manager-role/#returning-from-time-off) guidance
- [ ] Assign back to yourself and remove others
- [ ] Review any Retrospective Items and update [this template](https://gitlab.com/gitlab-com/Product/-/blob/main/.gitlab/issue_templates/PM-Coverage.md)
