:lock: This issue is only visible to [TEAM] team members.

## Context

Team members are the most important part of GitLab, and we do [Engagement Surveys](https://about.gitlab.com/handbook/people-group/engagement/) to gather feedback on what's going well and what we can improve on.

On 2022-05-16 we launched the FY23-Q2 Pulse Engagement Survey with 43 questions partnering with CultureAmp.
 
Remember, the survey is just a tool to help guide our focus, our discussions, and to track our progress.


## What to do?
We received the results, and now it's time to share them with everyone so we can all play a part in looking at where we need to adjust and continue to build on our plans for change.

1. Respond in line in the threads with suggestions and thoughts.
1. Vote to select 1-2 focus areas. To vote, use :point_up: on any of the threads.
 
This discussion is **not** meant to require anyone to reveal how they answered any question. The survey was anonymous, and people are welcome/expected to maintain anonymity as they choose.

### Does and dont's
- When discussing feedback, use "I" statements over "you" and "we" statements.
- It is tempting to want to find out "who said what" please set this desire aside and instead focus your attention on high-level themes with a goal of action and next steps

## Manager tasks
<details><summary>Click to expand</summary>

Spend time fully understanding your feedback: https://support.cultureamp.com/hc/en-us/articles/207302419-Engagement-Results-to-Action-Guide-For-Managers
- [ ] Participation. Do the results represent the views of most people? 
- [ ] Engagement score. What is the outcome we're driving towards and how are we doing?
- [ ] Comparisons. How are we tracking compared to others?
- [ ] Other factors. What are the key themes emerging in the results?
- [ ] What are we doing well?
- [ ] What are our high scores compared to others?
- [ ] What are our low scores compared to others?
- [ ] Take note of results that matched your expectations and results that surprised you
- [ ] Any polarizing questions (with a large favorable and unfavorable population)?
- [ ] Where's the biggest impact? What is most important to our people when it comes to our Key Factor?
- [ ] So what do we focus on?

Discuss the results with your team
- [ ] Create threads to share results. Any format is good, you may want to use Participation, Key Factor, Other Factors, and Questions
- [ ] Create a thread to discuss expectations vs surprises. Highlighting where the results aligned with your expectations and where they surprised you may make others feel open to sharing their own thoughts
- [ ] Create discussion threads for the areas you want to focus. Maybe you want to focus on the top items with negative change.

Add some ground rules on how the conversation should happen
- [ ] Thank your team for providing valuable feedback on how the organization can improve.
- [ ] Remind folks that you will be discussing perceptions. While others' perceptions may not always be the same as ours, everyone's perceptions are valid.
- [ ] Assure team members that this is a safe and confidential forum to express themselves and expand on their experiences.
- [ ] Call out that while it is tempting to want to find out "who said what", ask everyone to set this desire aside and instead focus their attention on high-level themes with a goal of action and next steps.
- [ ] Encourage team members to speak for themselves when discussing feedback by using "I" statements over "you" and "we" statements.

</details>
