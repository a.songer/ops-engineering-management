### Ops Section FY[xx]-Q[y] quarterly OKR plan

#### Schedule

| OKR Schedule | Week of | Task |
| :----------- | :------ | :--- |
| -5 | 2022-pp-qq | Kickoff.  Quick sync to review schedule and expectations. Director Ops and direct reports. |
| -4 | 2022-pp-qq | Planning - Initial Review Session - Director, SEMs, EMs - Review CEO, CTO, CProjO and VP Development OKR drafts |
| -3 | 2022-pp-qq | Planning - 50 minute Director/SEM OKR review meeting. Ops Directors+, Sr. EMs, Group PMs propose OKRs for their groups in Working Drafts ([in this doc](https://docs.google.com/document/d/1Auon_HTohhEdJiCJdXg_e8uHOHZapGmTSU5YPb58Chg/edit#) (internal)) |
| -2 | 2022-pp-qq | Planning - 50 minute SEM/EM meeting for Verify and CD compartment.  EMs propose OKRs for their groups via issues linked from review meeting doc. |
| -1 | 2022-pp-qq | OKRs are moved into Ally. Directors, SEMs, EMs document how to achieve following guidance in https://about.gitlab.com/company/okrs/#documenting-how-to-achieve |
| -1 | 2022-pp-qq | OKRs and how to achieve docs are shared by DRIs in #doe-ops and other appropriate slack channels. |
| 0  | 2022-pp-qq | Ops Sub-Dept OKR AMA.  25 minute kickoff meeting to discuss OKRs and answer questions. |

#### Tasks
- [ ] Name this issue for the quarter we're planning. Example: `Ops Section FY24-Q1 quarterly OKR plan`
- [ ] Schedule meetings listed above and invite the team members referenced in the table.
- [ ] Update the table above with the dates of the meetings.
- [ ] Add a comment to this issue for gather retrospective thoughts: `What went well`, `What went not so well`, `What can we improve`, and `Action items`
- [ ] Add labels to this issue for the quarter we're planning for.

/label ~section::ops ~OKR  
/CC @sgoldstein @cheryl.li @dcroft @kencjohnston @jreporter @kbychu
